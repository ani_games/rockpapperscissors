﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyTestTask.RPS.Model;
using MyTestTask.RPS.Parameters;

namespace MyTestTask.RPS
{

    /// <summary>
    /// Game entry point.
    /// Game State Management.
    /// </summary>

    public class GameManager : MonoBehaviour
    {
        public event System.Action OnGameStarted,OnGameFinished;
        public event System.Action<RoundResult> OnRoundFinished;
        public event System.Action<GameState> OnGameStateChanged;

        //Access point to all games entities
        public static GameManager Instance { get; private set; } 

        public GameSettings GameSettings { get; private set; }

        public GameScore TotalGameScore { get; private set; }
        public GameRound CurrentRound { get; private set; }

        GameState currentState;
        public GameState CurrentState
        {
            get { return currentState; }
            set
            {
                currentState = value;
                if (OnGameStateChanged != null)
                    OnGameStateChanged(value);
            }
        }

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                if (Instance != this)
                    DestroyImmediate(gameObject);
            }
            GameSettings = Resources.Load<GameSettings>("GameSettings");
            currentState = GameState.MainMenu;
        }

        void OnDestroy()
        {
            Instance = null;
        }


        public void FinishRound(RoundResult result)
        {
            TotalGameScore.FinishRound(result);
            CurrentState = GameState.ScoreScreen;            
            if(OnRoundFinished!=null)
                OnRoundFinished(result);
        }

        public void StartNewGame()
        {
            CurrentRound = new GameRound(GameSettings);
            TotalGameScore = new GameScore(GameSettings);
            CurrentState = GameState.RoundStart;
            if (OnGameStarted != null)
                OnGameStarted();
        }

        public void NextRound()
        {
            CurrentRound = new GameRound(GameSettings);
            CurrentState = GameState.RoundStart;
        }
    }
}
