﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Model
{
    [System.Serializable]
    public class Score
    {

        public int Points { get { return points; } }

        [SerializeField]
        int points;

        public Score(int points)
        {
            this.points = points;
        }

        public void AddScore(Score score)
        {
            points += score.Points;
        }

        public void ResetScore()
        {
            points = 0;
        }

    }
}