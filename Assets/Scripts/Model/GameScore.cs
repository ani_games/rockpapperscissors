﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyTestTask.RPS.Parameters;

namespace MyTestTask.RPS.Model
{
    public class GameScore
    {
        public delegate void ScoreViewListener(Score playerScore, Score opponentScore);
        ScoreViewListener scoreViewListeners;

        GameSettings gameSettings;

        public Score PlayerScore { get; private set; }
        public Score OpponentScore { get; private set; }

        public int RoundsComplete { get; private set; }

        public GameScore(GameSettings gameSettings)
        {
            this.gameSettings = gameSettings;
            PlayerScore = new Score(0);
            OpponentScore = new Score(0);

        }

        //TO DO: Вынести все в интерфейс
        public void AddScoreListener(ScoreViewListener scoreViewUpdate)
        {
            scoreViewListeners += scoreViewUpdate;
            scoreViewUpdate(PlayerScore, OpponentScore);
        }

        public void RemoveListener(ScoreViewListener scoreViewUpdate)
        {
            scoreViewListeners -= scoreViewUpdate;
        }

        public void RemoveAllListeners()
        {
            scoreViewListeners = null;
        }

        public void FinishRound(RoundResult result)
        {
            switch (result.Winner)
            {
                case RoundResult.WinnerType.Player:
                    PlayerScore.AddScore(gameSettings.WinScore);
                    OpponentScore.AddScore(gameSettings.DefeatScore);
                    break;
                case RoundResult.WinnerType.AIOpponent:
                    PlayerScore.AddScore(gameSettings.DefeatScore);
                    OpponentScore.AddScore(gameSettings.WinScore);
                    break;
                case RoundResult.WinnerType.Draw:
                    PlayerScore.AddScore(gameSettings.DrawScore);
                    OpponentScore.AddScore(gameSettings.DrawScore);
                    break;
            }
            RoundsComplete++;
            scoreViewListeners.Invoke(PlayerScore, OpponentScore);
        }
    }
}