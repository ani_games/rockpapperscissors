﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyTestTask.RPS.Parameters;

namespace MyTestTask.RPS.Model
{
    /// <summary>
    /// Round Model
    /// </summary>
    public class GameRound
    {

        public event System.Action OnGameRoundStarted;
        public event System.Action<Bet> OnPlayerBetSelected;
        public event System.Action<Bet> OnOpponentBetGenerated;
        public event System.Action<RoundResult> OnRoundResultCalculated;

        public Bet PlayerBet { get; private set; }
        public Bet OpponentBet { get; private set; }
        public RoundResult Result { get; private set; }

        GameSettings gameSettings;

        public GameRound(GameSettings gameSettings)
        {
            this.gameSettings = gameSettings;
        }

        public void StartRound()
        {
            if (OnGameRoundStarted != null)
                OnGameRoundStarted();
        }

        public void SelectPlayerBet(Bet bet)
        {
            Debug.Log("Player selected " + bet.Name);
            PlayerBet = bet;
            if (OnPlayerBetSelected != null)
                OnPlayerBetSelected(bet);
        }

        public void GenerateOpponentSelection()
        {
            RuleBase rule = gameSettings.Rule;
            if (rule == null)
            {
                Debug.LogError("There is no Rule with current number in Game Settings");
                return;
            }

            BetList bets = Resources.Load<BetList>("BetList");

            OpponentBet = rule.GetBet(bets, PlayerBet);

            Debug.Log("Opponent bet: " + OpponentBet.Name);

            if (OnOpponentBetGenerated != null)
                OnOpponentBetGenerated(OpponentBet);
        }

        public void Calculate()
        {
            if (PlayerBet.Rate == OpponentBet.Rate)
            {
                Result = RoundResult.Draw();
                PublicResult(Result);
                return;
            }

            Bet[] bets = Resources.Load<BetList>("BetList").betList;
            
            int max = 0, min =0;
            foreach (Bet bet in bets)
            {
                
                if (bet.Rate < min)
                    min = bet.Rate;
                else if (bet.Rate > max)
                    max = bet.Rate;
            }

            if (PlayerBet.Rate == min || PlayerBet.Rate == max)
            {
                if (PlayerBet.Rate > OpponentBet.Rate)
                {
                    if (OpponentBet.Rate == min)
                        Result = RoundResult.OpponentWinner();
                    else
                        Result = RoundResult.PlayerWinner();
                }
                else
                {
                    if (OpponentBet.Rate == max)
                        Result = RoundResult.PlayerWinner();
                    else
                        Result = RoundResult.OpponentWinner();

                }
            }
            else
            {
                if (PlayerBet.Rate > OpponentBet.Rate)
                    Result = RoundResult.PlayerWinner();
                else
                    Result = RoundResult.OpponentWinner();
            }

            PublicResult(Result);
        }

        public void PublicResult(RoundResult result)
        {
            if (OnRoundResultCalculated != null)
                OnRoundResultCalculated(result);
        }


    }
}