﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Model
{

    public class RoundResult
    {

        public WinnerType Winner { get; private set; }

        private RoundResult(WinnerType type)
        {
            Winner = type;
        }

        public static RoundResult PlayerWinner()
        {
            return new RoundResult(WinnerType.Player);
        }

        public static RoundResult OpponentWinner()
        {
            return new RoundResult(WinnerType.AIOpponent);
        }

        public static RoundResult Draw()
        {
            return new RoundResult(WinnerType.Draw);
        }

        public enum WinnerType
        {
            Player,
            AIOpponent,
            Draw
        }
    }
}
