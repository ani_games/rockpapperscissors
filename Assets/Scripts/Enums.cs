﻿namespace MyTestTask.RPS
{

    public enum GameState
    {
        MainMenu,
        RoundStart,
        InputWaiting,
        Calculating,
        RoundResult,
        ScoreScreen
    }

}