﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Controller
{

    public class UIController : MonoBehaviour
    {

        [SerializeField]
        GameObject UIMainMenu, UIRoundStart, UIInputWaiting, UICalculating, UIRoundResult, UIScoreScreen;

        Dictionary<GameState, GameObject> screens;

        void Start()
        {
            screens = new Dictionary<GameState, GameObject>();
            screens.Add(GameState.MainMenu, UIMainMenu);
            screens.Add(GameState.RoundStart, UIRoundStart);
            screens.Add(GameState.InputWaiting, UIInputWaiting);
            screens.Add(GameState.Calculating, UICalculating);
            screens.Add(GameState.RoundResult, UIRoundResult);
            screens.Add(GameState.ScoreScreen, UIScoreScreen);

            GameManager.Instance.OnGameStateChanged += SwitchScreen;
            SwitchScreen(GameState.MainMenu);
        }

        void OnDestroy()
        {
            if (GameManager.Instance != null)
            {
                GameManager.Instance.OnGameStateChanged -= SwitchScreen;
            }
        }


        void SwitchScreen(GameState state)
        {
            foreach (KeyValuePair<GameState, GameObject> screen in screens)
            {
                if (screen.Key == state)
                    screen.Value.SetActive(true);
                else
                    screen.Value.SetActive(false);
            }
        }
    }
}