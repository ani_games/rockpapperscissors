﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Parameters
{

    public class DishonestRule : RuleBase
    {

#pragma warning disable
        [SerializeField]
        [Range(0f,1f)]
        float chance = 0.5f;
#pragma warning restore

        public override Bet GetBet(BetList betList, Bet playerBet)
        {

            Bet[] bets = Resources.Load<BetList>("BetList").betList;
            Dictionary<int, Bet> ratedBets = new Dictionary<int, Bet>();
            int max = 0, min = 0;
            foreach (Bet bet in bets)
            {
                ratedBets.Add(bet.Rate, bet);
                if (bet.Rate < min)
                    min = bet.Rate;
                else if (bet.Rate > max)
                    max = bet.Rate;
            }

            if (UnityEngine.Random.Range(0f, 1f) < chance)
            {
                //Player Won
                if (playerBet.Rate == min)
                    return ratedBets[max];
                else
                    return ratedBets[playerBet.Rate - 1];
            }
            else
            {
                //Opponent Won
                if (playerBet.Rate == max)
                    return ratedBets[min];
                else
                    return ratedBets[playerBet.Rate + 1];
            }
        }
    }
}