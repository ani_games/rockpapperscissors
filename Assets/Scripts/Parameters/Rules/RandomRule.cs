﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Parameters
{
    public class RandomRule : RuleBase
    {
        public override Bet GetBet(BetList betList, Bet playerBet)
        {
            return betList.betList[UnityEngine.Random.Range(0, betList.betList.Length)];
        }
    }
}