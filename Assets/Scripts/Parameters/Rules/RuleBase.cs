﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Parameters
{

    public abstract class RuleBase : ScriptableObject
    {
        public abstract Bet GetBet(BetList betList, Bet playerBet);
    }
}