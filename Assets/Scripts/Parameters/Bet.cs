﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Parameters
{

    [System.Serializable]
    public class Bet
    {
#pragma warning disable
        [SerializeField]
        string key;
        [SerializeField]
        string name;
        [SerializeField]
        string animationEnding;
        [SerializeField]
        [Tooltip("Bigger wins smaller. Max Loses Min")]
        int rate;
        [SerializeField]
        Sprite icon;
#pragma warning disable

        public string Key { get { return key; } }
        public string Name { get { return name; } }
        public string AnimationEnding { get { return animationEnding; } }
        public int Rate { get { return rate; } }
        public Sprite Icon { get { return icon; } }

        /// <summary>
        /// Creating only in Scriptable Object
        /// </summary>
        private Bet()
        { }
    }
}