﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyTestTask.RPS.Model;

namespace MyTestTask.RPS.Parameters
{

    public class GameSettings : ScriptableObject
    {
#pragma warning disable
        [SerializeField]
        Score winScore, drawScore, defeatScore;
        [SerializeField]
        string playerName, opponentName;
        [Space(10)]
        [SerializeField]
        RuleBase[] rules;
        [SerializeField]
        int ruleSelected=0;
#pragma warning restore

        public Score WinScore { get { return winScore; } }
        public Score DrawScore { get { return drawScore; } }
        public Score DefeatScore { get { return defeatScore; } }

        public string PlayerName { get { return playerName; } }
        public string OpponentName { get { return opponentName; } }

        public RuleBase Rule
        {
            get
            {
                if (ruleSelected < rules.Length)
                    return rules[ruleSelected];
                else
                    return null;
            }
        }
    }
}