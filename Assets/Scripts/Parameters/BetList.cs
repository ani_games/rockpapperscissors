﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.Parameters
{

    public class BetList : ScriptableObject
    {
#pragma warning disable
        [SerializeField]
        public Bet[] betList;
#pragma warning restore
    }
}