﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyTestTask.RPS.View
{

    public class UIStartRound : MonoBehaviour
    {
#pragma warning disable
        [SerializeField]
        float delay = 1;
#pragma warning restore

        void OnEnable()
        {
            if (GameManager.Instance != null && GameManager.Instance.CurrentState == GameState.RoundStart)
                Invoke("Skip", delay);
        }


        void Skip()
        {
            GameManager.Instance.CurrentRound.StartRound();
            GameManager.Instance.CurrentState = GameState.InputWaiting;    
        }
    }
}