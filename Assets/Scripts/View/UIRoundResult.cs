﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyTestTask.RPS.View
{

    public class UIRoundResult : MonoBehaviour
    {
#pragma warning disable
        [SerializeField]
        GameObject victory, draw, defeat;
        [SerializeField]
        float delay = 2;
#pragma warning restore

        void OnEnable()
        {

            if (GameManager.Instance != null && GameManager.Instance.CurrentState == GameState.RoundResult)
            {

                switch (GameManager.Instance.CurrentRound.Result.Winner)
                {
                    case Model.RoundResult.WinnerType.Player:
                        victory.SetActive(true);
                        draw.SetActive(false);
                        defeat.SetActive(false);
                        break;
                    case Model.RoundResult.WinnerType.AIOpponent:
                        victory.SetActive(false);
                        draw.SetActive(false);
                        defeat.SetActive(true);
                        break;
                    case Model.RoundResult.WinnerType.Draw:
                        victory.SetActive(false);
                        draw.SetActive(true);
                        defeat.SetActive(false);
                        break;
                }

                Invoke("Skip", delay);
            }
        }

        void Skip()
        {
            GameManager.Instance.FinishRound(GameManager.Instance.CurrentRound.Result);
        }

    }
}