﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyTestTask.RPS.Model;

namespace MyTestTask.RPS.View
{

    public class UIScore : MonoBehaviour
    {

#pragma warning disable
        [SerializeField]
        UnityEngine.UI.Text txtScore, txtPlayer, txtOpponent;
#pragma warning restore

        void Start()
        {
            txtPlayer.text = GameManager.Instance.GameSettings.PlayerName;
            txtOpponent.text = GameManager.Instance.GameSettings.OpponentName;
            GameManager.Instance.OnGameStarted += OnGameStarted;
            GameManager.Instance.OnGameFinished += OnGameFinished;
        }

        void OnDestroy()
        {
            if (GameManager.Instance != null)
            {
                GameManager.Instance.OnGameStarted -= OnGameStarted;
                GameManager.Instance.OnGameFinished -= OnGameFinished;
            }
        }

        private void OnGameStarted()
        {
            txtPlayer.text = GameManager.Instance.GameSettings.PlayerName;
            txtOpponent.text = GameManager.Instance.GameSettings.OpponentName;
            GameManager.Instance.TotalGameScore.AddScoreListener(OnScoreUpdate);
        }

        private void OnGameFinished()
        {
            GameManager.Instance.TotalGameScore.RemoveListener(OnScoreUpdate);
        }
        

        void OnScoreUpdate(Score playerScore, Score opponentScore)
        {
            txtScore.text = string.Format("{0} : {1}",playerScore.Points,opponentScore.Points);
        }
    }
}