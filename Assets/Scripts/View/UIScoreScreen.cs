﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyTestTask.RPS.View
{

    public class UIScoreScreen : MonoBehaviour
    {
        [SerializeField]
        Text txtScore;
        [SerializeField]
        Button btnNext;

        void OnEnable()
        {
            txtScore.text = string.Format("{0}:{1}", GameManager.Instance.TotalGameScore.PlayerScore.Points, GameManager.Instance.TotalGameScore.OpponentScore.Points);
            btnNext.onClick.AddListener(NextRound);
        }

        void OnDisable()
        {
            btnNext.onClick.RemoveAllListeners();
        }


        void NextRound()
        {
            GameManager.Instance.NextRound();
        }
    }
}