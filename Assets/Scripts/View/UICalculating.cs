﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyTestTask.RPS.View
{

    public class UICalculating : MonoBehaviour
    {

#pragma warning disable
        [SerializeField]
        Image imgPlayerBet, imgOpponentBet;
        [SerializeField]
        float delay = 2;
#pragma warning restore

        void OnEnable()
        {
            if (GameManager.Instance != null && GameManager.Instance.CurrentState == GameState.Calculating)
            {
                GameManager.Instance.CurrentRound.GenerateOpponentSelection();
                GameManager.Instance.CurrentRound.Calculate();
                imgPlayerBet.sprite = GameManager.Instance.CurrentRound.PlayerBet.Icon;
                imgOpponentBet.sprite = GameManager.Instance.CurrentRound.OpponentBet.Icon;
                Invoke("ShowResults", delay);
            }
        }

        void ShowResults()
        {
            GameManager.Instance.CurrentRound.Calculate();
            GameManager.Instance.CurrentState = GameState.RoundResult;
        }

    }
}