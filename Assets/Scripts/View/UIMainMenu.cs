﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyTestTask.RPS.View
{

    public class UIMainMenu : MonoBehaviour
    {
        [SerializeField]
        Button btnStart;

        void OnEnable()
        {
            btnStart.onClick.AddListener(StartGame);
        }

        void OnDisable()
        {
            btnStart.onClick.RemoveAllListeners();
        }

        void StartGame()
        {
            GameManager.Instance.StartNewGame();
        }
    }
}