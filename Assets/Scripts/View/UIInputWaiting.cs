﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MyTestTask.RPS.Parameters;

namespace MyTestTask.RPS.View
{

    public class UIInputWaiting : MonoBehaviour
    {

#pragma warning disable
        [SerializeField]
        Transform buttonRow;
#pragma warning restore

        BetList betList;

        void Start()
        {
            betList = Resources.Load<BetList>("BetList");
            foreach (Bet bet in betList.betList)
            {
                CreateButton(bet);
            }
        }

        void CreateButton(Bet bet)
        {
            GameObject go = new GameObject("btn"+bet.Name);
            go.transform.localScale = Vector3.one;
            go.transform.SetParent(buttonRow);
            Button button = go.AddComponent<Button>();
            Image img = go.AddComponent<Image>();
            img.sprite = bet.Icon;
            button.image = img;
            button.onClick.AddListener(() => { SelectBet(bet); });
        }

        void SelectBet(Bet bet)
        {
            GameManager.Instance.CurrentRound.SelectPlayerBet(bet);
            GameManager.Instance.CurrentState = GameState.Calculating;
        }

    }
}